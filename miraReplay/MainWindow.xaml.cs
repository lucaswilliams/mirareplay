﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace miraReplay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            if (LoadFromLocal.IsChecked == true)
            {
                //Load the list of meetings from the local device folder specified.
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170101" });
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170219" });
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170307" });
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170410" });
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170503" });
                MeetingList.Items.Add(new ListBoxItem(){Content = "H20170606" });
            }

            if (LoadFromAWS.IsChecked == true)
            {
                //Load the list of meetings from the AWS Storage account specified.
            }
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            //Get the folder that we're talking about from the ListBox.
            string selectedMeet = MeetingList.SelectedItems[0].ToString();
            string selectedType = MeetingList.SelectedItems[0].GetType().ToString();
            selectedMeet = selectedMeet.Replace(selectedType + ": ", "");
            MessageBox.Show(selectedMeet);
        }
    }
}
